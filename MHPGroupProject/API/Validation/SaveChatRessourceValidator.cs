﻿using API.Model.ResponseDTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Validation
{
	public class SaveChatRessourceValidator : AbstractValidator<SaveChatRessource>
	{
		public SaveChatRessourceValidator()
		{
			RuleFor(chat => chat.UserIDs).NotNull().WithMessage("You can't start a chat with less then two persons");
			RuleFor(chat => chat.UserIDs.Count()).GreaterThan(1).WithMessage("You must add atleast two or more users in a chat!");
		}
	}
}
