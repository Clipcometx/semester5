﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API.Model;

namespace API.Data
{
    public class MHPGroupContext : DbContext
    {
        public MHPGroupContext (DbContextOptions<MHPGroupContext> options)
            : base(options)
        {
        }

        public DbSet<API.Model.Chat> Chat { get; set; }

        public DbSet<API.Model.Comment> Comment { get; set; }

        public DbSet<API.Model.Message> Message { get; set; }

        public DbSet<API.Model.Post> Post { get; set; }

        public DbSet<API.Model.Tag> Tag { get; set; }

    }
}
