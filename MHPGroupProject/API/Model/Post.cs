﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Model
{
    public class Post
    {
        #region Private Fields

        #endregion

        #region Public Properties
        public int ID { get; set; }
        public List<Tag> Tags{ get; set; }
        public List<Comment> Comments { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserID { get; set; }
        [NotMapped]
        public User User { get; set; }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        #endregion

    }
}
