﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Model
{
    public class User : IdentityUser
    {
        #region Private Fields
        #endregion

        #region Public Properties
        [NotMapped]
        public string Token { get; set; }
        [NotMapped]
        public List<string> Roles { get; set; }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
