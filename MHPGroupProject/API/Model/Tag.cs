﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Tag
    {
        #region Private Fields

        #endregion

        #region Public Properties
        public int ID { get; set; }
        public string Name { get; set; }
        public List<Post> Posts { get; set; }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        #endregion
    }
}
