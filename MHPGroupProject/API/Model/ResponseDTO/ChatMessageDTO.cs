﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.ResponseDTO
{
    public class ChatMessageDTO
    {
        public ChatMessageDTO(string userID, string content, DateTime createdDate)
        {
            UserID = userID;
            Content = content;
            CreatedDate = createdDate;
        }

        public string Content { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
