﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.ResponseDTO
{
    public class ChatUserDTO
    {
        public string UserID { get; set; }

        public ChatUserDTO(string userID)
        {
            UserID = userID;
        }
    }
}
