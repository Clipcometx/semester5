﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.ResponseDTO
{
	public class ChatMessageResponse
	{
		public string StatusMessage { get; set; }
		public string Message { get; set; }
	}
}
