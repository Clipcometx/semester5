﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.ResponseDTO
{
	public class SaveChatMessageRessource
	{
		public string Message { get; set; }
		public string UserID { get; set; }
		public int ChatID { get; set; }
	}
}
