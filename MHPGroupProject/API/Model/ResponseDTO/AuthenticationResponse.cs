﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model.ResponseDTO
{
    public class AuthenticationResponse
    {
        public string Username { get; set; }
		[DataType(DataType.Password)]
		public string Pass { get; set; }
	}
}
