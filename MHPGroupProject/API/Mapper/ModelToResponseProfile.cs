﻿using API.Model;
using API.Model.ResponseDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Mapper
{
	public class ModelToResponseProfile : Profile
	{
		public ModelToResponseProfile()
		{
			CreateMap<User, UserResponse>();
			CreateMap<User, AuthenticationResponse>();
			CreateMap<User, SaveAuthenticationResponse>();
		}
	}
}
