﻿using API.Model;
using API.Model.ResponseDTO;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
    
        private readonly UserManager<User> _userManager;

        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;


        public AuthController(UserManager<User> userManager, IConfiguration configuration, RoleManager<IdentityRole> roleManager, IMapper mapper)
        {
            this._userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] SaveAuthenticationResponse model)
        {
            User user = new User()
            {
                Email = model.Username,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username,
                EmailConfirmed = true
            };

            /*Temporary Role assignment*/
            if(!(await _roleManager.RoleExistsAsync("Admin")))
            {
                IdentityRole role = new IdentityRole
                {
                    Name = "Admin"
                };
                var RoleResult = await _roleManager.CreateAsync(role);
                
            }

            var result = await _userManager.CreateAsync(user, model.Pass);

            if (result.Succeeded)
            {

                await _userManager.AddToRoleAsync(user, "Admin");

                return Ok();
            }

            //Account couldn't be created
            return NotFound();
            
        }

        [HttpPost]
        [Route("Authenticate")]
        public async Task<IActionResult> Login([FromBody] AuthenticationResponse model)
        {
            User user = await _userManager.FindByNameAsync(model.Username);
            
            if(user != null)
            {
                PasswordVerificationResult result = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Pass);

                if(result == PasswordVerificationResult.Success)
                {
                    List<string> Roles = (List<string>)await _userManager.GetRolesAsync(user);

                    user.Roles = Roles;

                    var token = GenerateToken(user);

                    user.Token = token;

                    UserResponse userResource = _mapper.Map<User, UserResponse>(user);

                    return Ok(userResource);
                }

            }

            //Login failed
            return NotFound();
        }

        [HttpPost]
        [Route("Logout")]
        public IActionResult LogOut([FromBody]User user)
        {
            //Invalidate user token
            //Might need to be done client side by deleting the token from user and maybe blacklisting the token
            //https://social.msdn.microsoft.com/Forums/en-US/b1c9ab84-a0ef-4231-bf11-02bf4a0b7786/logout-in-jwt-aspnet-core?forum=aspwebapi
            return BadRequest();
        }

        //Method for Token Generation on login
        private string GenerateToken(IdentityUser identityUser)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var k = _configuration.GetSection("Token:Token").Value;
            var key = Encoding.ASCII.GetBytes(_configuration.GetSection("Token:Token").Value);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, identityUser.UserName.ToString()),
                    new Claim(ClaimTypes.Email, identityUser.Email)
                }),
                Audience = "MIG",
                Issuer = "MIG2",
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        } 

    }
}
