﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Model;
using API.Model.ResponseDTO;
using API.Validation;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChatsController : ControllerBase
    {
        private readonly MHPGroupContext _context;

        public ChatsController(MHPGroupContext context)
        {
            _context = context;
        }

        // GET: api/Chats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Chat>>> GetChat()
        {
            return await _context.Chat.ToListAsync();
        }

        // GET: api/Chats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Chat>> GetChat(int id)
        {
            var chat = await _context.Chat.FindAsync(id);

            if (chat == null)
            {
                return NotFound();
            }

            return chat;
        }

        // PUT: api/Chats/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChat(int id, Chat chat)
        {
            if (id != chat.ID)
            {
                return BadRequest();
            }

            _context.Entry(chat).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Chats
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ChatResponse>> PostChat(SaveChatRessource saveChatRessource)
        {
            SaveChatRessourceValidator validator = new SaveChatRessourceValidator();
            ValidationResult results = validator.Validate(saveChatRessource);

			if (!results.IsValid)
			{
                return BadRequest(results.Errors);
			}

            Chat chat = new Chat();
            chat.Users = new List<ChatUser>();
			foreach (string userId in saveChatRessource.UserIDs)
			{
                chat.Users.Add(new ChatUser { UserID = userId }) ;
			}

            _context.Chat.Add(chat);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChat", new { id = chat.ID }, chat);
        }

        [HttpGet]
        [Route("FindChats")]
        public async Task<ActionResult<List<ChatResponse>>> FindChats(string UserID)
        {
            List<ChatResponse> chats = await _context.Chat
                .Where(x => x.Users.Any(x => x.UserID == UserID))
                .Select(x => new ChatResponse {
                    Id = x.ID,
                    ChatUsers = x.Users.Select(y => new ChatUserDTO(y.UserID)).ToList(),
                    Messages = x.Messages.Select(i => new ChatMessageDTO(i.UserID, i.Content, i.CreatedDate)).ToList()
                }
            ).ToListAsync();


            if (chats != null)
            {
                return Ok(chats);
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("FindSpecificChat")]
        public async Task<ActionResult<ChatResponse>> FindSpecificChat(int chatID)
        {
            ChatResponse chats = await _context.Chat
                .Where(y => y.ID == chatID)
                .Select(x => new ChatResponse
                {
                    Id = x.ID,
                    ChatUsers = x.Users.Select(y => new ChatUserDTO(y.UserID)).ToList(),
                    Messages = x.Messages.Select(i => new ChatMessageDTO(i.UserID, i.Content, i.CreatedDate)).ToList()
                }
            ).FirstAsync();


            if (chats != null)
            {
                return Ok(chats);
            }

            return BadRequest();
        }




        [HttpPost]
        [Route("SendMessage")]
        public async Task<ActionResult<ChatMessageResponse>> SendMessage(SaveChatMessageRessource saveChatMessage)
		{
            Chat chat = await _context.Chat.Include(x=>x.Messages).Where(x => x.ID == saveChatMessage.ChatID).FirstOrDefaultAsync();

			if (chat == null)
			{
                return NotFound();
			}
           
            chat.Messages.Add(new Message { Content = saveChatMessage.Message, UserID = saveChatMessage.UserID });
            await _context.SaveChangesAsync();

            return Ok();
		}

        // DELETE: api/Chats/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChat(int id)
        {
            var chat = await _context.Chat.FindAsync(id);
            if (chat == null)
            {
                return NotFound();
            }

            _context.Chat.Remove(chat);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ChatExists(int id)
        {
            return _context.Chat.Any(e => e.ID == id);
        }
    }
}
