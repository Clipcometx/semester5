﻿using API.Model;
using API.Model.ResponseDTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class UserController : Controller
	{
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
		public UserController(UserManager<User> userManager, IMapper mapper)
		{
            _userManager = userManager;
            _mapper = mapper;
		}
        // GET: UserController
        [HttpPost]
        [Route("DeleteUser")]
        public async Task<IActionResult> DeleteUser([FromBody] User user)
        {
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return Ok();
                }
                //Add Error handling with result.Errors
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("FriendList")]
        public async Task<ActionResult<List<UserResponse>>> GetFriendList([FromBody] User CurrentUser)
        {
            List<User> users = await _userManager.Users.Where(x => x.Id != CurrentUser.Id).ToListAsync();
           
            List<UserResponse> usersDTO = new List<UserResponse>();
            foreach(User user in users)
            {
                usersDTO.Add(_mapper.Map<User, UserResponse>(user));
            }

            return Ok(usersDTO);
        }

        [HttpPost]
        [Route("FindUserByUsername")]
        public async Task<IActionResult> FindUserByName(string Username)
        {

            if (!string.IsNullOrEmpty(Username))
            {
                User result = await _userManager.FindByNameAsync(Username);
                if (result != null)
                {

                    List<string> Roles = (List<string>)await _userManager.GetRolesAsync(result);

                    result.Roles = Roles;

                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }

            return BadRequest();

        }

        [HttpPost]
        [Route("FindUserByID")]
        public async Task<IActionResult> FindUserByID(string UserID)
        {
            if (!string.IsNullOrEmpty(UserID))
            {
                User result = await _userManager.FindByIdAsync(UserID);
                if (result != null)
                {
                    List<string> Roles = (List<string>)await _userManager.GetRolesAsync(result);

                    result.Roles = Roles;

                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }

            return BadRequest();

        }
    }
}
