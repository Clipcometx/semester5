﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Model
{
    public class Chat
    {

        public int ID { get; set; }
        public List<Message> Messages { get; set; }
        public List<ChatUser> Users{ get; set; }
    }
}
