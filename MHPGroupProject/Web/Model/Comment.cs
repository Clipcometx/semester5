﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Model
{
    public class Comment
    {
        #region Private Fields

        #endregion

        #region Public Properties
        public int ID { get; set; }
        public string Content { get; set; }
        public Post Post { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int UserID { get; set; }
        [NotMapped]
        public User User { get; set; }
        #endregion

        #region Private Methods

        #endregion

        #region Public Methods

        #endregion
    }
}
