﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Model
{
    public class Message
    {
        #region Private Fields
        #endregion

        #region Public Properties
        public int ID { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public Chat Chat { get; set; }
        public int UserID { get; set; }
        [NotMapped]
        public User User { get; set; }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #endregion
    }
}
