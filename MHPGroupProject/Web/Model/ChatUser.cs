﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Model
{
    public class ChatUser
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public Chat Chat { get; set; }
        [NotMapped]
        public User User { get; set; }
    }
}
