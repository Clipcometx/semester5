﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Model
{
    public static class CurrentUser
    {
        public static User User { get; set; } = null;
    }
}
