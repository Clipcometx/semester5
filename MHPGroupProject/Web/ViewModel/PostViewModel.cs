﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Model;

namespace Web.ViewModel
{
	public class PostViewModel
	{
		public List<TagViewModel> Tags { get; set; }
		public string Content { get; set; }
		public UserViewModel UserViewModel { get; set; }
		public DateTime CreatedDate { get; set; }
		public List<CommentViewModel> Comments { get; set; }
		public PostViewModel(string content, List<TagViewModel> tagViewModels, UserViewModel userViewModel, DateTime createdDate, List<CommentViewModel> commentViewModels)
		{
			Content = content;
			UserViewModel = userViewModel;
			CreatedDate = createdDate;
			Tags = tagViewModels;
			Comments = commentViewModels;
		}
	}
}
