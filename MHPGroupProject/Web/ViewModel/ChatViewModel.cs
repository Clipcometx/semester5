﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
using Web.Model;

namespace Web.ViewModel
{
	public class ChatViewModel
	{
        public ChatResponse Chat { get; set; }
        public SendMessageDTO SendMessage { get; set; }
    }
}
