﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;

namespace Web.ViewModel
{
    public class UserListViewModel
    {
        public CreateChatDTO CreateChatDTO { get; set; }
        public List<UserResponse> Users { get; set; }
        public string ChosenUser { get; set; }
        public List<ChatResponse> ExistingChats { get; set; }
        public int SelectedChat { get; set; }
    }
}
