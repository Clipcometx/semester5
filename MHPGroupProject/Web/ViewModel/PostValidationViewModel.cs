﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Model;

namespace Web.ViewModel
{
	public class PostValidationViewModel
	{
		public string Tags { get; set; }
		public string Content { get; set; }
		public string UserID { get; set; }
	}
}
