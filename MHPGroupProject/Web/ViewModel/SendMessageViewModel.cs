﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Model;

namespace Web.ViewModel
{
    public class SendMessageViewModel
    {
        public string Message { get; set; }
        public int ChatID { get; set; }
    }
}
