﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Model;

namespace Web.ViewModel
{
	public class UserViewModel
	{
		public string UserID { get; set; }
		public string UserName { get; set; }
		public UserViewModel(User user)
		{
			UserName = user.UserName;
		}
	}
}
