﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModel
{
	public class CommentViewModel
	{
		public string Content { get; }
		public string UserName { get; }
		public DateTime CreatedDate { get; set; }
		public CommentViewModel(string content, string userName, DateTime createdDate)
		{
			Content = content;
			UserName = userName;
			CreatedDate = createdDate;
		}
	}
}
