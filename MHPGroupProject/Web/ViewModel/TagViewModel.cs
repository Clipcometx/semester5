﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModel
{
	public class TagViewModel
	{
		public TagViewModel(string name)
		{
			Name = name;
		}

		public string Name { get; set; }
	}
}
