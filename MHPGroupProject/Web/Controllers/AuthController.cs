﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Services;
using Web.Model;

namespace Web.Controllers
{
	public class AuthController : Controller
	{
		public IActionResult Index()
		{
			return null;
		}

		public IActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Login(string returnUrl, AuthUser user)
		{
			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());

			User baseuser = await mHPAPI.Authenticate(user.Username, user.Pass);
            if (baseuser != null)
            {
				CurrentUser.User = baseuser;
            }
			return LocalRedirect(returnUrl);
		}
		[HttpGet]
		public async Task<IActionResult> Register()
		{
			return View();
		}
		[HttpPost]
		public async Task<IActionResult> Register(string returnUrl, AuthUser user)
		{
			try
			{
				MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());
				User newUser = await mHPAPI.RegisterUser(user);
				User baseuser = await mHPAPI.Authenticate(user.Username, user.Pass);

				if (baseuser != null)
				{
					CurrentUser.User = baseuser;
				}
			}
			catch (Exception)
			{
			}
			
			return LocalRedirect(returnUrl);
		}

		[HttpGet]
		public async Task<IActionResult> Logout()
		{
			CurrentUser.User = null;
			return LocalRedirect("/");
		}
	}
}
