﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
using Web.Model;
using Web.Services;
using Web.ViewModel;

namespace Web.Controllers
{
	public class HomeController : Controller
	{
		public async Task<IActionResult> Index()
		{
			//Return created chats for userlist
			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());

			List<UserResponse> users = await mHPAPI.GetFriendList();

			UserListViewModel userListViewModel = new UserListViewModel();
			userListViewModel.Users = users;

			List<ChatResponse> chats = await mHPAPI.FindChats();

			userListViewModel.ExistingChats = chats;

			ViewData["UserList"] = userListViewModel;

			return View();
		}

		public IActionResult Privacy()
		{
			return View();
		}
	}
}
