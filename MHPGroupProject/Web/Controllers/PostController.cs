﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Model;
using Web.Services;
using Web.ViewModel;

namespace Web.Controllers
{
	public class PostController : Controller
	{
		public async Task<IActionResult> IndexAsync()
		{
			if (CurrentUser.User == null || !CurrentUser.User.Roles.Any(x => x == "Admin"))
			{
				return LocalRedirect("/");
			}

			List<PostViewModel> postViewModels = null;
			try
			{
				MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());
				List<Post> posts = await mHPAPI.GetPosts();
				postViewModels = posts
					.Select(x => new PostViewModel ( 
						x.Content, 
						x.Tags.Select(t => new TagViewModel(t.Name)).ToList(), 
						new UserViewModel(mHPAPI.GetUserById(x.UserID).Result),
						x.CreatedDate,
						x.Comments.Select(c => new CommentViewModel(c.Content, c.User.UserName, c.CreatedDateTime)).ToList()
					)).ToList();
			}
			catch (Exception)
			{

			}
			return View(postViewModels);
		}

		[HttpGet]
		public async Task<IActionResult> CreatePost()
		{
			if (CurrentUser.User == null || !CurrentUser.User.Roles.Any(x => x == "Admin"))
			{
				return LocalRedirect("/");
			}
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> CreatePost(string returnUrl, PostValidationViewModel postValidationViewModel)
		{
			if (CurrentUser.User == null || !CurrentUser.User.Roles.Any(x => x == "Admin"))
			{
				return LocalRedirect("/");
			}
			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());
			try
			{
				Post post = await mHPAPI.CreatePost(postValidationViewModel);
			}
			catch (Exception)
			{
				
			}
			
			return LocalRedirect(returnUrl);
		}
	}
}
