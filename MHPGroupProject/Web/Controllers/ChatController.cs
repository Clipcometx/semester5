﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.DTO;
using Web.Model;
using Web.Services;
using Web.ViewModel;

namespace Web.Controllers
{
	public class ChatController : Controller
	{
		public IActionResult OpenChat()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> OpenChat(UserListViewModel userListViewModel)
        {

			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());


			//Get users for userlist
			List<UserResponse> users = await mHPAPI.GetFriendList();

			userListViewModel.Users = users;

			List<ChatResponse> chats = await mHPAPI.FindChats();

			userListViewModel.ExistingChats = chats;

			ViewData["UserList"] = userListViewModel;


			//Find Chat from id
			ChatResponse chat = await mHPAPI.FindSpecificChat(userListViewModel.SelectedChat);

			ChatViewModel chatViewModel = new ChatViewModel
			{
				Chat = chat
			};


			return View(chatViewModel);
        }

		[HttpGet]
		public async Task<IActionResult> CreateChat()
		{
			if (CurrentUser.User == null || !CurrentUser.User.Roles.Any(x => x == "Admin"))
			{
				return LocalRedirect("/");
			}


			return View();
		}

		[HttpPost]
		public async Task<IActionResult> CreateChat(UserListViewModel userListViewModel)
		{
			if (CurrentUser.User == null || !CurrentUser.User.Roles.Any(x => x == "Admin"))
			{
				return LocalRedirect("/");
			}

			List<string> userIds = new List<string>();

			userIds.Add(CurrentUser.User.Id);
			userIds.Add(userListViewModel.ChosenUser);

			CreateChatDTO createChat = new CreateChatDTO()
			{
				UserIds = userIds
			};
			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());
			try
			{
				Chat chat = await mHPAPI.CreateChat(createChat);
			}
			catch (Exception)
			{

			}

			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public async Task<IActionResult> SendMessage(string returnUrl, ChatViewModel chatViewModel, string Message)
        {
			chatViewModel.SendMessage.Message = Message;
			MHPAPI mHPAPI = new MHPAPI("https://localhost:44353", new System.Net.Http.HttpClient());

			try
            {
				Message message = await mHPAPI.SendMessage(chatViewModel.SendMessage);

			}
            catch
            {

            }

			return LocalRedirect(returnUrl);
		}

	}
}
