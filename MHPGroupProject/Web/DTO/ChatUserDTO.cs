﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class ChatUserDTO
    {
        public string UserID { get; set; }
    }
}
