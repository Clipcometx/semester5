﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class SendMessageDTO
    {
        public string Message { get; set; }
        public string UserID { get; set; }
        public int ChatID { get; set; }
    }
}
