﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class CreateChatDTO
    {
        public List<string> UserIds { get; set; }
    }
}
