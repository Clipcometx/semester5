﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class ChatResponse
    {
        public int Id { get; set; }
        public List<ChatUserDTO> ChatUsers { get; set; }
        public List<ChatMessageDTO> Messages { get; set; }
    }
}
