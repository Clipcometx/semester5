﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class UserResponse
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string Id { get; set; }
        public List<string> Roles { get; set; }
    }
}
