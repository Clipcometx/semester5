﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.DTO
{
    public class ChatMessageDTO
    {
        public string Content { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
