﻿using Web.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using Web.ViewModel;
using Web.DTO;

namespace Web.Services
{
    public class MHPAPI
    {
        private string _baseURL;
        private HttpClient _httpClient;

        public MHPAPI(string baseURL, HttpClient httpClient)
        {
            _baseURL = baseURL;
            _httpClient = httpClient;
        }

        public async Task<User> Authenticate(string username,string password)
        {
            HttpClient httpClient = new HttpClient();
            object obj = new {Username = username,Pass = password};
            var stringContent = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(_baseURL + "/api/Auth/Authenticate", stringContent);

            HttpContent requestContent = response.Content;
            string jsonContent = await requestContent.ReadAsStringAsync();
            User user = JsonConvert.DeserializeObject<User>(jsonContent);

            return user;
        }
        public async Task<User> RegisterUser(AuthUser authUser)
		{
            HttpClient httpClient = new HttpClient();
            var stringContent = new StringContent(JsonConvert.SerializeObject(authUser), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(_baseURL + "/api/Auth/Register", stringContent);

            HttpContent requestContent = response.Content;
            string jsonContent = await requestContent.ReadAsStringAsync();
            User user = JsonConvert.DeserializeObject<User>(jsonContent);

            return user;
        }

        public async Task<Post> CreatePost(PostValidationViewModel postViewModel)
		{
            List<Tag> tags = new List<Tag>();
			foreach (string tagName in postViewModel.Tags.Split(','))
			{
                tags.Add(new Tag() { Name = tagName });
			}
            Post newPost = new Post
            {
                Content = postViewModel.Content,
                UserID = CurrentUser.User.Id,
                Tags = tags,
                CreatedDate = DateTime.Now
            };
            var httpContent = new StringContent(JsonConvert.SerializeObject(newPost), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.PostAsync(_baseURL + "/api/Posts", httpContent);
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            Post responsePost = JsonConvert.DeserializeObject<Post>(responseJsonContent);

            return responsePost;
        }

        public async Task<List<Post>> GetPosts()
		{
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.GetAsync(_baseURL + "/api/Posts");
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            List<Post> responsePosts = JsonConvert.DeserializeObject<List<Post>>(responseJsonContent);

            return responsePosts;
        }

        public async Task<Chat> CreateChat(CreateChatDTO createChatDTO)
		{
            var httpContent = new StringContent(JsonConvert.SerializeObject(createChatDTO), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.PostAsync(_baseURL + "/api/Chats", httpContent);
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            Chat responseChat = JsonConvert.DeserializeObject<Chat>(responseJsonContent);

            return responseChat;
        }


        public async Task<Message> SendMessage(SendMessageDTO sendMessageDTO)
        {
            var httpContent = new StringContent(JsonConvert.SerializeObject(sendMessageDTO), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.PostAsync(_baseURL + "/api/Chats/SendMessage", httpContent);
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            Message responseMessage = JsonConvert.DeserializeObject<Message>(responseJsonContent);

            return responseMessage;
        }


        public async Task<User> GetUserById(string userID)
		{
            var httpContent = new StringContent(userID, Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.PostAsync(_baseURL + "/api/Auth/FindUserByID", httpContent);
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            User responseUser = JsonConvert.DeserializeObject<User>(responseJsonContent);

            return responseUser;
        }

        public async Task<List<UserResponse>> GetFriendList()
        {
            var httpContent = new StringContent(JsonConvert.SerializeObject(CurrentUser.User), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.PostAsync(_baseURL + "/api/User/FriendList", httpContent);
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            List<UserResponse> responseUser = JsonConvert.DeserializeObject<List<UserResponse>>(responseJsonContent);

            return responseUser;
        }

        public async Task<List<ChatResponse>> FindChats()
        {
            //var httpContent = new StringContent(JsonConvert.SerializeObject(CurrentUser.User), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.GetAsync(_baseURL + $"/api/Chats/FindChats?UserID={CurrentUser.User.Id}");
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            List<ChatResponse> responseChat = JsonConvert.DeserializeObject<List<ChatResponse>>(responseJsonContent);

            return responseChat;
        }

        public async Task<ChatResponse> FindSpecificChat(int chatID)
        {
            //var httpContent = new StringContent(JsonConvert.SerializeObject(CurrentUser.User), Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", CurrentUser.User.Token);

            HttpResponseMessage response = await _httpClient.GetAsync(_baseURL + $"/api/Chats/FindSpecificChat?ChatID={chatID}");
            HttpContent requestContent = response.Content;
            string responseJsonContent = await requestContent.ReadAsStringAsync();

            ChatResponse responseChat = JsonConvert.DeserializeObject<ChatResponse>(responseJsonContent);

            return responseChat;
        }
    }
}
